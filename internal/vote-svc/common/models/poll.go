package models

type KeyItem int

type Poll struct {
	Items map[KeyItem]Item `json:"items"`
}

type Item struct {
	Name       string `json:"Name"`
	NumberVote int    `json:"NumberVote"`
}

func (i *Item) Vote() {
	i.NumberVote += 1
}
