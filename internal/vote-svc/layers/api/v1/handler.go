package api

import (
	"fmt"
	"net/http"
)

func NewCastomHandler() http.Handler {
	return http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		if request.URL.Path != "/hello" {
			http.Error(writer, "404 not found.", http.StatusNotFound)
		}

		if request.Method != "GET" {
			http.Error(writer, "404 not found.", http.StatusNotFound)
		}

		fmt.Fprintf(writer, "Hello!")
	})
}
