package controllers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"lab1/internal/vote-svc/common/models"
	"lab1/internal/vote-svc/common/models/service"
	"net/http"
)

type VoteController struct {
	appVote service.AppPoll
}

func NewVoteController(appVote service.AppPoll) *VoteController {
	return &VoteController{
		appVote: appVote,
	}
}

func (v *VoteController) CreatePoll(writer http.ResponseWriter, request *http.Request) { //(pollID models.KeyItem, items []models.Item) error{
	body := request.Body
	data, err := ioutil.ReadAll(body)
	if err != nil {
		err = fmt.Errorf("error with creating poll: %v", err)
		http.Error(writer, "error with creating poll", http.StatusBadGateway)
	}
	var result map[string]iterface{}
	err = json.Unmarshal(data, &result)
	if err != nil{
		err = fmt.Errorf("error with creating poll: %v", err)
		http.Error(writer, "error with creating poll", http.StatusBadGateway)
	}
	pollID := result["poll_id"].(models.KeyItem)
	items := result["items"].([]models.Item)
	err  := v.appVote.CreatePoll( pollID, items)
	if err != nil {
		err = fmt.Errorf("error with creating poll: %v", err)
		http.Error(writer, "error with creating poll", http.StatusBadGateway)
	}
}
func (v *VoteController) VoteForItem(writer http.ResponseWriter, request *http.Request) { //(pollID models.KeyItem, ItemID models.KeyItem) error{

}
func (v *VoteController) GetResult(writer http.ResponseWriter, request *http.Request) { //(pollID models.KeyItem) (leaderBoard []models.Item, err error){

}