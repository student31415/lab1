package main

import (
	"fmt"
	"lab1/internal/vote-svc/common/models"
	"lab1/internal/vote-svc/layers/application"
)

const serverAddr = ":8085"

func main() {

	//serverPublic := http.Server{
	//	Addr:    serverAddr,
	//	Handler: api.NewCastomHandler(),
	//}
	//
	//fmt.Printf("Starting server at port %s\n", serverAddr)
	//if err := serverPublic.ListenAndServe(); err != nil {
	//	//http.ListenAndServe(":8085", nil); err != nil {
	//	log.Fatalln(err)
	//}

	voteSvc := application.NewVoteSvc()

	var pollID models.KeyItem = 0
	items := []models.Item{
		{
			Name: "item-69",
		},
		{
			Name: "item-21",
		},
	}

	err := voteSvc.CreatePoll(pollID, items)
	if err != nil {
		fmt.Println(err)
	}

	err = voteSvc.VoteForItem(pollID, models.KeyItem(0))
	if err != nil {
		fmt.Println(err)
	}

	err = voteSvc.VoteForItem(pollID, models.KeyItem(0))
	if err != nil {
		fmt.Println(err)
	}

	err = voteSvc.VoteForItem(pollID, models.KeyItem(1))
	if err != nil {
		fmt.Println(err)
	}

	result, err := voteSvc.GetResult(pollID)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(result)
}
