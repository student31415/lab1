package service

import (
	"lab1/internal/vote-svc/common/models"
)

type AppPoll interface {
	CreatePoll(pollID models.KeyItem, items []models.Item) error
	VoteForItem(pollID models.KeyItem, ItemID models.KeyItem) error
	GetResult(pollID models.KeyItem) (leaderBoard []models.Item, err error)
}
