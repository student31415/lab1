package application

import (
	"fmt"
	"lab1/internal/vote-svc/common/models"
	"lab1/internal/vote-svc/common/models/service"
)

type VoteSvc struct {
	polls map[models.KeyItem]*models.Poll
}

var _ service.AppPoll = (*VoteSvc)(nil)

func NewVoteSvc() *VoteSvc {
	return &VoteSvc{
		polls: make(map[models.KeyItem]*models.Poll),
	}
}

func (v *VoteSvc) CreatePoll(pollID models.KeyItem, items []models.Item) error {
	poll := &models.Poll{Items: make(map[models.KeyItem]models.Item)}
	for i, item := range items {
		poll.Items[models.KeyItem(i)] = item
	}
	if _, ok := v.polls[pollID]; ok {
		return fmt.Errorf("poll with polle_id  '%v' already exsists", pollID)
	}
	v.polls[pollID] = poll
	return nil
}

func (v *VoteSvc) VoteForItem(pollID models.KeyItem, ItemID models.KeyItem) error {
	poll, ok := v.polls[pollID]
	if !ok {
		return fmt.Errorf("poll with polle_id  '%v' does not exsists ", pollID)
	}
	item, ok := poll.Items[ItemID]
	if !ok {
		return fmt.Errorf("pokemon with item_id  '%v' does not exsists in poll with poll_id '%v'", ItemID, pollID)
	}
	item.Vote()
	poll.Items[ItemID] = item

	return nil
}

func (v *VoteSvc) GetResult(pollID models.KeyItem) (leaderBoard []models.Item, err error) {
	poll, ok := v.polls[pollID]
	if !ok {
		err = fmt.Errorf("poll with polle_id  '%v' does not exsists ", pollID)
		return
	}

	leaderBoard = make([]models.Item, 0, len(poll.Items))
	for _, item := range poll.Items {
		leaderBoard = append(leaderBoard, item)
	}
	return
}
